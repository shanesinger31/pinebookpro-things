# Maintainer: Daniel Peukert <daniel@peukert.cc>
# Contributor: Christoph Mohr <christoph.mohr@gmail.com>
# Contributor: Giovanni 'ItachiSan' Santini <giovannisantini93@yahoo.it>
# Contributor: Filipe Laíns (FFY00) <lains@archlinux.org>
# Contributor: Pieter Goetschalckx <3.14.e.ter <at> gmail <dot> com>
_pkgname='ferdi'
pkgname="$_pkgname-git"
pkgver=5.6.1.nightly.6.r1.g34dc3a6d
pkgrel=1
# epoch=1
pkgdesc='A messaging browser that allows you to combine your favorite messaging services into one application - git version'
arch=('x86_64' 'i686' 'armv7h' 'aarch64')
url="https://get$_pkgname.com"
license=('Apache')
depends=('electron' 'libxkbfile')
makedepends=('git' 'nodejs' 'npm6' 'python' 'python2' 'nvm')
provides=("$_pkgname")
conflicts=("$_pkgname")
_nodeversion=14.17.3
source=(
    "$pkgname::git+https://github.com/get$_pkgname/$_pkgname"
    "$pkgname-recipes::git+https://github.com/get$_pkgname/recipes"
    "$pkgname-internal-server::git+https://github.com/get$_pkgname/internal-server"
    'fix-autostart-path.diff'
    'remove-meetfranz-unpack.diff'
)
sha256sums=('SKIP'
            'SKIP'
            'SKIP'
            '1b332afa1276449ca1bfd387ad8a9b28024269a4d66daa030b0944e874df24c1'
            'aa06840b98231a7fa3ece7239ba721459f5c6ecd4148d7e0ec4deb716c61ab48')

_sourcedirectory="$pkgname"
_homedirectory="$pkgname-home"

case "$CARCH" in
    i686)
        _electronbuilderarch='ia32'
    ;;
    armv7h)
        _electronbuilderarch='armv7l'
    ;;
    aarch64)
        _electronbuilderarch='arm64'
    ;;
    *)
        _electronbuilderarch='x64'
    ;;
esac

prepare() {
    cd "$srcdir/$_sourcedirectory/"
    export npm_config_cache="${srcdir}/npm_cache"
    _ensure_local_nvm
    nvm install ${_nodeversion}

    # Provide git submodules
    git submodule init
    git config submodule.recipes.url "$srcdir/$pkgname-recipes"
    git config submodule.src/internal-server.url "$srcdir/$pkgname-internal-server"
    git submodule update --init --recursive

    # Set system Electron version for ABI compatibility
    sed -E -i 's|("electron": ").*"|\1'"$(cat '/usr/lib/electron/version')"'"|' 'package.json'

    # Loosen node version restriction
    sed -E -i 's|("node": ").*"|\1'"$(node --version | sed 's/^v//')"'"|' 'package.json'

    # Specify path for autostart file
    patch --forward -p1 < '../fix-autostart-path.diff'

    # Remove asarUnpack rule for @meetfranz packages
    patch --forward -p1 < '../remove-meetfranz-unpack.diff'

    # Build recipe archives
    cd "$srcdir/$_sourcedirectory/recipes/"
    HOME="$srcdir/$_homedirectory" npm install
    HOME="$srcdir/$_homedirectory" npm run package

    # Prepare dependencies
    cd "$srcdir/$_sourcedirectory/"
    HOME="$srcdir/$_homedirectory" npx lerna bootstrap
}

pkgver() {
    cd "$srcdir/$_sourcedirectory/"
    git describe --long --tags | sed -e 's/^v//' -e 's/-\([^-]*-g[^-]*\)$/-r\1/' -e 's/-/./g'
}

build() {
    cd "$srcdir/$_sourcedirectory/"
    export npm_config_cache="${srcdir}/npm_cache"
    _ensure_local_nvm
    nvm use ${_nodeversion}

    NODE_ENV='production' HOME="$srcdir/$_homedirectory" npx gulp build
    NODE_ENV='production' HOME="$srcdir/$_homedirectory" npx electron-builder --linux dir "--$_electronbuilderarch" -c.electronDist='/usr/lib/electron' -c.electronVersion="$(cat '/usr/lib/electron/version')"
}

package() {
    cd "$srcdir/$_sourcedirectory/"

    local _outpath='out/linux'
    if [ "$_electronbuilderarch" != 'x64' ]; then
        _outpath="$_outpath-$_electronbuilderarch"
    fi
    _outpath="$_outpath-unpacked"

    function remove_srcdir_ref {
        local tmppackage="$(mktemp)"
        jq '.|=with_entries(select(.key|test("_.+")|not))' "$1" > "$tmppackage"
        mv "$tmppackage" "$1"
        chmod 644 "$1"
    }

    # Remove $srcdir references - https://wiki.archlinux.org/index.php/Node.js_package_guidelines
    for i in $(find "$pkgdir" -name package.json); do
        remove_srcdir_ref $i
    done

    install -Dm644 "$_outpath/resources/app.asar" "$pkgdir/usr/lib/$_pkgname/app.asar"
    install -dm755 "$pkgdir/usr/lib/$_pkgname/app.asar.unpacked/"
    cp -r --no-preserve=ownership --preserve=mode "$_outpath/resources/app.asar.unpacked/recipes/" "$pkgdir/usr/lib/$_pkgname/app.asar.unpacked/recipes/"

    install -dm755 "$pkgdir/usr/bin/"
    cat << EOF > "$pkgdir/usr/bin/$_pkgname"
#!/bin/sh
NODE_ENV=production ELECTRON_IS_DEV=0 exec electron '/usr/lib/$_pkgname/app.asar' "\$@"
EOF
    chmod +x "$pkgdir/usr/bin/$_pkgname"

    install -dm755 "$pkgdir/usr/share/applications/"
    cat << EOF > "$pkgdir/usr/share/applications/$_pkgname.desktop"
[Desktop Entry]
Name=${_pkgname^}
Exec=/usr/bin/$_pkgname %U
Terminal=false
Type=Application
Icon=$_pkgname
StartupWMClass=${_pkgname^}
Comment=Ferdi is your messaging app / former Emperor of Austria and combines chat & messaging services into one application. Ferdi currently supports Slack, WhatsApp, WeChat, HipChat, Facebook Messenger, Telegram, Google Hangouts, GroupMe, Skype and many more. You can download Ferdi for free for Mac & Windows.
MimeType=x-scheme-handler/ferdi;
Categories=Network;InstantMessaging;
EOF

    for _size in 16 24 32 48 64 96 128 256 512 1024; do
        install -Dm644 "build-helpers/images/icons/${_size}x${_size}.png" "$pkgdir/usr/share/icons/hicolor/${_size}x${_size}/apps/$_pkgname.png"
    done
}

# https://wiki.archlinux.org/title/Node.js_package_guidelines#Using_nvm
_ensure_local_nvm() {
    # let's be sure we are starting clean
    which nvm >/dev/null 2>&1 && nvm deactivate && nvm unload
    export NVM_DIR="${srcdir}/.nvm"

    # The init script returns 3 if version specified
    # in ./.nvrc is not (yet) installed in $NVM_DIR
    # but nvm itself still gets loaded ok
    source /usr/share/nvm/init-nvm.sh || [[ $? != 1 ]]
}
