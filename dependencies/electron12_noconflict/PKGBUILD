# Maintainer: Dan Johansen <strit@manjaro.org>

pkgname=electron12-noconflict
_pkgname=electron12
pkgver=12.0.9
pkgrel=1
pkgdesc='Build cross platform desktop apps with web technologies - version: 12, non-conflicting'
arch=('aarch64')
url='https://electronjs.org/'
license=('MIT' 'custom')
depends=('c-ares' 'ffmpeg' 'gtk3' 'http-parser' 'libevent' 'libnghttp2'
         'libxslt' 'libxss' 'minizip' 'nss' 're2' 'snappy')
optdepends=('kde-cli-tools: file deletion support (kioclient5)'
            'trash-cli: file deletion support (trash-put)'
            "xdg-utils: open URLs with desktop's default (xdg-email, xdg-open)")
provides=("electron12")
conflicts=("electron12")
source=("https://github.com/electron/electron/releases/download/v${pkgver}/electron-v${pkgver}-linux-arm64.zip"
        "electron12.desktop")
md5sums=('81e23dec3b12afe41db35580838d59f4'
         'cf0639e52586143bbb6c92813bd40873')

package() {
  install -dm755 "${pkgdir}/usr/lib/${_pkgname}"
  cp -a "${srcdir}"/* "${pkgdir}/usr/lib/${_pkgname}"

  install -dm755 "${pkgdir}/usr/share/licenses/${_pkgname}"
  for l in "${pkgdir}/usr/lib/${_pkgname}"/{LICENSE,LICENSES.chromium.html}; do
    ln -s  \
      $(realpath --relative-to="${pkgdir}/usr/share/licenses/${_pkgname}" ${l}) \
      "${pkgdir}/usr/share/licenses/${_pkgname}"
  done

  install -dm755 "${pkgdir}"/usr/bin
  ln -s ../lib/${_pkgname}/electron "${pkgdir}"/usr/bin/${_pkgname}

  # Install .desktop and icon file (see default_app-icon.patch)
  install -Dm644 -t "${pkgdir}/usr/share/applications" ${_pkgname}.desktop
}
