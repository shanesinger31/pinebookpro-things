# Encrypted root on PBP

## extlinux.conf

The `extlinux.conf` shows an example config using the recent uboot on Manjaro that parses `/boot/extlinux.conf` instead of `/boot/boot.scr`.

The `dracut` command I've used for this config:

```bash
sudo dracut -H --force -a "crypt lvm drm" -o "kernel-network-modules kernel-modules-extra kernel-modules mdraid qemu qemu-net \
lunmask dmraid btrfs modsign i18n" --drivers="rockchipdrm drm drm_kms_helper analogix_dp panel-simple pwm_bl" \
/boot/initramfs-dracut.img
```

An example for the older uboot (also using `mkinitcpio`) can still be found in `old_uboot`.

